#pragma once
#include <cstdint>
#include <string>

namespace frame_topic {
  static const std::string interior_env = "interior_env";
}

struct interior_env_frame_t {
  float temperature;
  float humidity;

  interior_env_frame_t() {}

  interior_env_frame_t(
      float temperature,
      float humidity
  ) 
  : temperature(temperature)
  , humidity(humidity)
  {}
};

#ifdef QT_CORE_LIB
#include <QMetaType>
Q_DECLARE_METATYPE(interior_env_frame_t)
#endif
