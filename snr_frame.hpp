#pragma once
#include <cstdint>
#include <string>

namespace frame_topic {
  static const std::string snr = "snr";
}

struct snr_frame_t {
  int32_t snr;

  snr_frame_t() {}
  snr_frame_t(
    int32_t snr
  )
  : snr(snr)
  {}
};

#ifdef QT_CORE_LIB
#include <QMetaType>
Q_DECLARE_METATYPE(snr_frame_t)
#endif
